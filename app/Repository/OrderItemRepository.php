<?php

namespace App\Repository;

use App\Models\OrderItem;

final class OrderItemRepository
{
    public function getById(int $id): OrderItem
    {
        return OrderItem::findOrFail($id);
    }

    public function getByProductId(int $productId, int $orderId): ?OrderItem
    {
        return OrderItem::where('product_id', $productId)
            ->where('order_id', $orderId)
            ->first();
    }

    public function save(OrderItem $orderItem): OrderItem
    {
        $orderItem->save();

        return $orderItem;
    }

    public function delete(OrderItem $orderItem): ?bool
    {
        return $orderItem->delete();
    }
}
