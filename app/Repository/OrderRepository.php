<?php

namespace App\Repository;

use App\Models\Order;

final class OrderRepository
{
    public function getById(int $id): Order
    {
        return Order::findOrFail($id);
    }

    public function save(Order $order): Order
    {
        $order->save();

        return $order;
    }

    public function delete(Order $order): ?bool
    {
        return $order->delete();
    }
}
