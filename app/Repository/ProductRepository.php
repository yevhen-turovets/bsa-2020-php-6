<?php

namespace App\Repository;

use App\Models\Product;

final class ProductRepository
{
    public function getById(int $id): Product
    {
        return Product::findOrFail($id);
    }
}
