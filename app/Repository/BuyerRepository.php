<?php

namespace App\Repository;

use App\Models\Buyer;

final class BuyerRepository
{
    public function getById(int $id): Buyer
    {
        return Buyer::findOrFail($id);
    }
}
