<?php

namespace App\Http\Request;

final class AddOrderHttpRequest extends ApiFormRequest
{
    public function rules(): array
    {
        return [
            'buyerId' => 'required|integer|min:1',
            'orderItems.*.productId' => 'required|integer|min:1|exists:my_products,id',
            'orderItems.*.productDiscount' => 'required|integer|min:0|max:99',
            'orderItems.*.productQty' => 'required|integer|min:1',
        ];
    }
}
