<?php

namespace App\Http\Presenter;

use App\Models\Order;
use Illuminate\Support\Collection;

final class OrderAsArrayPresenter implements CollectionAsArrayPresenter
{
    private $buyerAsArrayPresenter;
    private $orderItemsAsArrayPresenter;

    public function __construct(
        BuyerAsArrayPresenter $buyerAsArrayPresenter,
        OrderItemAsArrayPresenter $orderItemsAsArrayPresenter
    ) {
        $this->buyerAsArrayPresenter = $buyerAsArrayPresenter;
        $this->orderItemsAsArrayPresenter = $orderItemsAsArrayPresenter;
    }

    public function present(Order $order): array
    {
        return [
            'orderId' => $order->getId(),
            'date' => $order->getDate(),
            'orderItems' => $this->orderItemsAsArrayPresenter->presentCollection($order->orderItems),
            'buyerId' => $this->buyerAsArrayPresenter->present($order->buyer)
        ];
    }

    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Order $order) {
                    return $this->present($order);
                }
            )
            ->all();
    }
}
