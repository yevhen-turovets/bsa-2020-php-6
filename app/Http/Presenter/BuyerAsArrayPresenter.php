<?php

namespace App\Http\Presenter;

use App\Models\Buyer;
use Illuminate\Support\Collection;

final class BuyerAsArrayPresenter implements CollectionAsArrayPresenter
{
    public function present(Buyer $buyer): array
    {
        return [
            'buyerFullName' => $buyer->getName() . " " . $buyer->getSurname(),
            'buyerAddress' => $buyer->getCountry() . " " . $buyer->getCity() . " " . $buyer->getAddressLine(),
            'buyerPhone' => $buyer->getPhone()
        ];
    }

    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Buyer $buyer) {
                    return $this->present($buyer);
                }
            )
            ->all();
    }
}
