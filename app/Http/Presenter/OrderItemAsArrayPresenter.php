<?php

namespace App\Http\Presenter;

use App\Models\OrderItem;
use Illuminate\Support\Collection;

final class OrderItemAsArrayPresenter implements CollectionAsArrayPresenter
{
    public function present(OrderItem $orderItem): array
    {
        return [
            'productName' => $orderItem->product->name,
            'productQty' => $orderItem->getQuantity(),
            'productPrice' => $orderItem->getPriceInDollar(),
            'productDiscount' => $orderItem->getDiscount() . "%",
            'productSum' => number_format($orderItem->getSum(), 2)
        ];
    }

    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (OrderItem $orderItem) {
                    return $this->present($orderItem);
                }
            )
            ->all();
    }
}
