<?php

namespace App\Http\Controllers;

use App\Action\Order\AddOrderAction;
use App\Action\Order\AddOrderRequest;
use App\Action\Order\DeleteOrderAction;
use App\Action\Order\DeleteOrderRequest;
use App\Action\Order\GetByIdRequest;
use App\Action\Order\GetOrderByIdAction;
use App\Action\Order\UpdateOrderAction;
use App\Action\Order\UpdateOrderRequest;
use App\Http\Presenter\OrderAsArrayPresenter;
use App\Http\Request\AddOrderHttpRequest;
use App\Http\Request\UpdateOrderHttpRequest;
use App\Models\Order;

class OrderController extends Controller
{
    private $addOrderAction;
    private $updateOrderAction;
    private $getOrderByIdAction;
    private $deleteOrderAction;
    private $presenter;

    public function __construct(
        AddOrderAction $addOrderAction,
        DeleteOrderAction $deleteOrderAction,
        UpdateOrderAction $updateOrderAction,
        GetOrderByIdAction $getOrderByIdAction,
        OrderAsArrayPresenter $presenter
    ) {
        $this->addOrderAction = $addOrderAction;
        $this->updateOrderAction = $updateOrderAction;
        $this->getOrderByIdAction = $getOrderByIdAction;
        $this->deleteOrderAction = $deleteOrderAction;
        $this->presenter = $presenter;
    }

    public function index()
    {
        $order = Order::all();

        return $this->presenter->presentCollection($order);
    }

    public function store(AddOrderHttpRequest $request)
    {
        $response = $this->addOrderAction->execute(
            new AddOrderRequest(
                (int)$request->get('buyerId'),
                (array)$request->get('orderItems')
            )
        );

        return $this->presenter->present($response->getOrder());
    }

    public function show($id)
    {
        $order = $this->getOrderByIdAction->execute(
            new GetByIdRequest((int)$id)
        )->getOrder();

        return $this->presenter->present($order);
    }

    public function update(UpdateOrderHttpRequest $request, $id)
    {
        $response = $this->updateOrderAction->execute(
            new UpdateOrderRequest(
                (int)$id,
                (array)$request->get('orderItems')
            )
        );

        return $this->presenter->present($response->getOrder());
    }

    public function destroy($id)
    {
        $this->deleteOrderAction->execute(
            new DeleteOrderRequest((int)$id)
        );

        return 'Deleted';
    }
}
