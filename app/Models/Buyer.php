<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Buyer extends Model
{
    protected $table = 'buyers';
    protected $primaryKey = 'id';

    protected $with = ['orders'];

    protected $fillable = [
        'name',
        'surname',
        'country',
        'city',
        'address_line',
        'phone',
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'buyer_id', 'id');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getAddressLine(): string
    {
        return $this->address_line;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
