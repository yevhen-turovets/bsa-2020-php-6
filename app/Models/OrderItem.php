<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'price',
        'discount'
    ];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getPriceInDollar(): float
    {
        return $this->price/100;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getSum(): float
    {
        if ($this->discount) {
            return ($this->getPriceInDollar() - $this->getPriceInDollar()*($this->discount)*0.01)*$this->quantity;
        } else {
           return $this->quantity*$this->getPriceInDollar();
        }
    }
}
