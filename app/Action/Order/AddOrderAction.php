<?php

namespace App\Action\Order;

use App\Exceptions\ProductNotFoundException;
use App\Models\Order;
use App\Exceptions\BuyerNotFoundException;
use App\Models\OrderItem;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use App\Repository\BuyerRepository;
use App\Repository\ProductRepository;
use Carbon\Carbon;

final class AddOrderAction
{
    private $buyerRepository;
    private $orderRepository;
    private $orderItemRepository;
    private $productRepository;

    public function __construct(
        BuyerRepository $buyerRepository,
        OrderRepository $orderRepository,
        OrderItemRepository $orderItemRepository,
        ProductRepository $productRepository
    ) {
        $this->buyerRepository = $buyerRepository;
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->productRepository = $productRepository;
    }

    public function execute(AddOrderRequest $request): AddOrderResponse
    {
        try {
            $this->buyerRepository->getById($request->getBuyerId());
        } catch (BuyerNotFoundException $ex) {
            throw new BuyerNotFoundException();
        }

        $order = new Order();
        $order->date = Carbon::now();
        $order->buyer_id = $request->getBuyerId();

        $order = $this->orderRepository->save($order);

        foreach ($request->getOrderItems() as $orderItem) {
            $this->orderItemSave($orderItem, $order->getId());
        }

        return new AddOrderResponse($order);
    }

    private function orderItemSave($orderItem, $orderId)
    {
        $productId = $orderItem['productId'];
        $productQty = $orderItem['productQty'];
        $productDiscount = $orderItem['productDiscount'];

        try {
            $product = $this->productRepository->getById($productId);
        } catch (ProductNotFoundException $ex) {
            throw new ProductNotFoundException();
        }

        $orderItem = new OrderItem();
        $orderItem->order_id = $orderId;
        $orderItem->product_id = $productId;
        $orderItem->quantity = $productQty;
        $orderItem->price = $product->getPrice();
        $orderItem->discount = $productDiscount;

        $this->orderItemRepository->save($orderItem);
    }
}
