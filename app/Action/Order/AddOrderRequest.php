<?php

namespace App\Action\Order;

final class AddOrderRequest
{
    private $buyerId;
    private $orderItems;

    public function __construct(int $buyerId, array $orderItems)
    {
        $this->buyerId = $buyerId;
        $this->orderItems = $orderItems;
    }

    public function getBuyerId(): int
    {
        return $this->buyerId;
    }

    public function getOrderItems(): array
    {
        return $this->orderItems;
    }
}
