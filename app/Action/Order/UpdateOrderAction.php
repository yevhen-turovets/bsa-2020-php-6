<?php

declare(strict_types=1);

namespace App\Action\Order;

use App\Exceptions\OrderNotFoundException;
use App\Exceptions\ProductNotFoundException;
use App\Models\OrderItem;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;

final class UpdateOrderAction
{
    private $orderRepository;
    private $orderItemRepository;
    private $productRepository;

    public function __construct(
        OrderRepository $orderRepository,
        OrderItemRepository $orderItemRepository,
        ProductRepository $productRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->productRepository = $productRepository;
    }

    public function execute(UpdateOrderRequest $request): UpdateOrderResponse
    {
        try {
            $order = $this->orderRepository->getById($request->getOrderId());
        } catch (OrderNotFoundException $ex) {
            throw new OrderNotFoundException();
        }

        foreach ($request->getOrderItems() as $orderItem) {
            $productId = $orderItem['productId'];
            $productQty = $orderItem['productQty'];
            $productDiscount = $orderItem['productDiscount'];

            try {
                $product = $this->productRepository->getById((int)$productId);
            } catch (ProductNotFoundException $ex) {
                throw new ProductNotFoundException();
            }

            $orderItem = $this->orderItemRepository->getByProductId((int)$productId, $order->getId());

            if(!$orderItem) {
                $orderItem = new OrderItem();
            }

            $orderItem->order_id = $order->getId();
            $orderItem->product_id = $productId;
            $orderItem->quantity = $productQty;
            $orderItem->price = $product->getPrice();
            $orderItem->discount = $productDiscount;

            $this->orderItemRepository->save($orderItem);
        }

        return new UpdateOrderResponse($order);
    }
}
