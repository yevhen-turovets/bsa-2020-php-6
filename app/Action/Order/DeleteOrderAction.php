<?php

declare(strict_types=1);

namespace App\Action\Order;

use App\Exceptions\OrderNotFoundException;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class DeleteOrderAction
{
    private $orderRepository;
    private $orderItemRepository;

    public function __construct(
        OrderRepository $orderRepository,
        OrderItemRepository $orderItemRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
    }

    public function execute(DeleteOrderRequest $request): void
    {
        try {
            $order = $this->orderRepository->getById($request->getId());
        } catch (ModelNotFoundException $ex) {
            throw new OrderNotFoundException();
        }

        $order->orderItems()->each(function ($orderItem) {
            $this->orderItemRepository->delete($orderItem);
        });

        $this->orderRepository->delete($order);
    }
}
