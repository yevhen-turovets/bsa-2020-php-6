<?php

namespace App\Action\Order;

final class UpdateOrderRequest
{
    private $orderId;
    private $orderItems;

    public function __construct(int $orderId, array $orderItems)
    {
        $this->orderId = $orderId;
        $this->orderItems = $orderItems;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getOrderItems(): array
    {
        return $this->orderItems;
    }
}
