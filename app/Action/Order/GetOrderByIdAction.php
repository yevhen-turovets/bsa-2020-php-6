<?php

namespace App\Action\Order;

use App\Repository\OrderRepository;

class GetOrderByIdAction
{
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(GetByIdRequest $request): GetOrderByIdResponse
    {
        $order = $this->repository->getById($request->getId());

        return new GetOrderByIdResponse($order);
    }
}
