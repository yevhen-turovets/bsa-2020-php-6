<?php

namespace App\Action\Order;

use App\Models\Order;

final class GetOrderByIdResponse
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }
}
