<?php

use Illuminate\Database\Seeder;
use App\Models\Buyer;
use App\Models\Order;

class BuyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Buyer::class, 10)->create()
            ->each(function ($buyer){
                $buyer->orders()->saveMany(
                    factory(Order::class, 5)
                        ->make()
                );
            });
    }
}
