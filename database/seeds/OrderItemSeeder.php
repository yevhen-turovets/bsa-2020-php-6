<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\OrderItem;

class OrderItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = Order::all();

        $order->map(
            function (Order $order) {
                factory(OrderItem::class, 3)->create([
                                                       'order_id' => $order->id,
                                                         ]);
            }
        );
    }
}
