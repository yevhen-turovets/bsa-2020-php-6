<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Buyer;
use Faker\Generator as Faker;

$factory->define(Buyer::class, function (Faker $faker) {
    $data = [
        'name' => $faker->firstName(),
        'surname' => $faker->lastName(),
        'country' => $faker->country(),
        'city' => $faker->city(),
        'address_line' => $faker->address(),
        'phone' => $faker->phoneNumber(),
    ];

    return $data;
});
