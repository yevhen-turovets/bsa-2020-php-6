<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    $data = [
        'date' => $faker->dateTimeBetween('-3 month','today'),
    ];

    return $data;
});
