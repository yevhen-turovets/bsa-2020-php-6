<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderItem;
use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    $product = Product::query()->inRandomOrder()->select('id','price')->first();

    $data = [
        'price' => $product->price,
        'quantity' => $faker->numberBetween(1, 100),
        'discount' => $faker->numberBetween(0, 99),
        'product_id' => $product->id,
    ];

    return $data;
});
